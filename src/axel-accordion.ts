import { AxelAccordionStatic } from "./axel";
import { AxelAccordionObject, AxelAccordionOptions, AxelAccordionUserOptions } from "./axel-types";

export class AxelAccordion extends AxelAccordionStatic {
  private static initialized: boolean = false

  static init(selector: string, userOptions: AxelAccordionUserOptions = {}): AxelAccordionObject {
    const defaultOptions: AxelAccordionOptions = {
      name: selector,
      allowMultiple: false,
      defaultOpenedTabs: []
    }
  
    const tabSelector = '[data-axel-accordion*="tab-"]'
    const controlSelector = '[data-axel-accordion="control"]'
    const panelSelector = '[data-axel-accordion="panel"]'

    // add styles on first tab group initialization
    if (!this.initialized) {
      const style = document.createElement('style')
      style.textContent = tabSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }'
      document.head.appendChild(style)
      this.initialized = true
    }
   
    const element = document.querySelector(selector) as HTMLDivElement
    const tabs = Array.from(element.querySelectorAll(tabSelector)) as HTMLDivElement[]
    const options: AxelAccordionOptions = { ...defaultOptions, ...userOptions }
    const tabsToOpen = options.defaultOpenedTabs

    const switchTab = (tabName: string) => {
			accordionObj.tabs.forEach(tab => {
        const openedTabNames = accordionObj.openedTabNames;

        // clicked tab
				if (tab.attributes.getNamedItem('data-axel-accordion')?.value === tabName) {
          if (tab.classList.contains('axel-active')) { 
            tab.classList.remove('axel-active')
            accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName)
          } else { 
            tab.classList.add('axel-active')
            openedTabNames.push(tabName)
          }

        // not a clicked tab
        } else {
          if (options.allowMultiple !== true) {
            tab.classList.remove('axel-active')
            accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName)
          }
        }
			})
		}

    const closeAllTabs = () => {
      accordionObj.tabs.forEach(tab => {
        tab.classList.remove('axel-active')
      })
      accordionObj.openedTabNames = []
    }

    // create object for html tab group
		const accordionObj: AxelAccordionObject = {
      element: element,
      tabs: tabs,
      options: options,
      openedTabNames: tabsToOpen,
			switchTab: switchTab,
      closeAllTabs: closeAllTabs
    }

		// set initial state
    tabsToOpen.forEach((tabName) => {
      switchTab(tabName)      
    })

    // append event listeners for controls
		accordionObj.tabs.forEach(tabEl => {
      const tabControl = tabEl.querySelector(controlSelector) as HTMLDivElement

      tabControl.addEventListener('click', () => {
        const tabName = tabEl.attributes.getNamedItem('data-axel-accordion')?.value

        accordionObj.switchTab(tabName as string)
      });
		})

    // add accordion object to the static `allAccordions` list
		this.allObjects.push(accordionObj)

		return accordionObj
  }

  static switchTab(accordionName: string, tabName: string) {
    const accordion = this.getObject(accordionName)
    accordion.switchTab(tabName)
  }

  static closeAllTabs(accordionName: string) {
    const accordion = this.getObject(accordionName)
    accordion.closeAllTabs()
  }
}