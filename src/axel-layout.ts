import { AxelLayoutStatic } from "./axel"
import { AxelLayoutObject, AxelLayoutOptions, AxelLayoutUserOptions } from "./axel-types"

/**
 * Initializes and controls HTML layout of the application.
 */
export class AxelLayout extends AxelLayoutStatic {
  static init(
    selector: string,
    userOptions: AxelLayoutUserOptions = {},
    onDebouncedResize = () => {}
  ): AxelLayoutObject {
    const defaultOptions: AxelLayoutOptions = {
      name: null,
      layoutMinHeight: null,
      contentElement: null,
      keepContentMinHeightEqualTo: null,
      debounceTimeInMs: 200
    }
    const options: AxelLayoutOptions = { ...defaultOptions, ...userOptions }

    const element = document.querySelector(selector) as HTMLElement
    const contentElement = options.contentElement ? document.querySelector(options.contentElement) as HTMLElement : element
    const measuredElement = options.keepContentMinHeightEqualTo ? document.querySelector(options.keepContentMinHeightEqualTo) as HTMLElement : null

    // create object for html layout
		const layoutObject: AxelLayoutObject = {
      element: element,
      options: options,
			contentElement: contentElement,
      onDebouncedResize: onDebouncedResize
    }

    const updateLayout = () => {
      if (layoutObject.options.layoutMinHeight === 'screen') {
        layoutObject.element.style.minHeight = window.innerHeight+'px'
      }
  
      if (typeof layoutObject.options.layoutMinHeight === 'number') {
        layoutObject.element.style.minHeight = options.layoutMinHeight+'px'
      }
  
      if (layoutObject.options.keepContentMinHeightEqualTo) {
        if (measuredElement) {
          layoutObject.contentElement.style.minHeight = this.computeElementHeight(measuredElement)+'px'
        }
      }
    }

    updateLayout();

    // update layout on resize - debounced
    let resizeTimeout: number;
    window.addEventListener('resize', (event) => {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(() => {
        updateLayout()
        layoutObject.onDebouncedResize(event)
      }, options.debounceTimeInMs)
    });

    // add layout object to the static `allObjects` list
		this.allObjects.push(layoutObject)

		return layoutObject
  }

  /**
   * Computes height of an element even if the element is hidden
   * 
   * @param element computed element
   * @returns height of the element in px
   */
  private static computeElementHeight = (element: HTMLElement) => {
    const isDisplayed = window.getComputedStyle(element).display !== 'none'

    if (isDisplayed) return element.offsetHeight

    const wrapper = document.createElement('div');
    wrapper.style.visibility = 'hidden !important'
    wrapper.style.overflow = 'hidden !important'
    wrapper.style.height = '0 !important'
    
    const clone = element.cloneNode(true) as HTMLElement
    clone.style.display = 'block'
    clone.style.height = 'auto'
    wrapper.appendChild(clone)

    element.parentElement?.appendChild(wrapper)
    const height = clone.offsetHeight
    element.parentElement?.removeChild(wrapper)

    return height
  }
}