import { AxelSelectStatic, onClickOutsideOf } from "./axel"
import { AxelSelectInitFn, AxelSelectObject, AxelSelectOptions, AxelSelectSwitchOptionFn, AxelSelectSwitchOptionsFn, AxelSelectSwitchOptionsStaticFn, AxelSelectSwitchOptionStaticFn, AxelSelectUserOptions } from "./axel-types"

/**
 * Initializes and controls list of selects.
 */
export class AxelSelect extends AxelSelectStatic {
  /**
   * Whether the first select has been initialized
   */
  private static initialized: boolean = false
  /**
   * Listeners reused on reinitializing
   */
  private static listeners = {
    onControlClick: (selectObj: AxelSelectObject) => () => {
      selectObj.element.classList.toggle('axel-active')
		},
    onOptionClick: (selectObj: AxelSelectObject, optionEl: HTMLElement, options: AxelSelectOptions) => () => {
      const optionNameToSwitch = optionEl.attributes.getNamedItem('data-axel-select')?.value
      if (!optionNameToSwitch) return;
    
      if (options.allowMultiple) {
        selectObj.switchOption(optionNameToSwitch, undefined, true)
      } else {
        selectObj.selectedOptions.forEach((optionName) => {
          if (optionName !== optionNameToSwitch) {
            selectObj.switchOption(optionName, false, true)
          }
        })
        selectObj.switchOption(optionNameToSwitch, true, true)
      }
      // close panel (on manual selection)
      selectObj.options.closeOnSelect && selectObj.element.classList.remove('axel-active')
    },
    onClickOutside: (selectObj: AxelSelectObject) => () => {
      selectObj.element.classList.remove('axel-active')
    }
  }
  /**
   * Initialize new select and add it to the static list
   * 
   * @param selector select element selector
   * @param userOptions allows to specify `name`, `allowMultiple` and `defaultSelectedOptions`
   * @returns object of a single select that has been initialized 
   */
  static init: AxelSelectInitFn = (
    selector: string,
    userOptions: AxelSelectUserOptions = {},
    onSwitch?: (
      optionName: string, 
      newState: boolean, 
      selectedOptions: string[],
      optionEl: HTMLDivElement
    ) => void 
  ) => {
    const defaultOptions: AxelSelectOptions = {
      name: selector,
      allowMultiple: true,
      closeOnSelect: true,
      textEmpty: '- select -',
      textFull: null,
      defaultSelectedOptions: []
    }

    const selectSelector = '[data-axel-select="select"]'
    const controlSelector = '[data-axel-select="control"]'
    const panelSelector = '[data-axel-select="panel"]'
    const valuesSelector = '[data-axel-select="values"]'
    const optionsSelectors = '[data-axel-select*="option-"]'

    // add styles on first select initialization
    if (!this.initialized) {
      const style = document.createElement('style')
      style.textContent = selectSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }'
      document.head.appendChild(style)
      this.initialized = true
    }

		const element = document.querySelector(selector) as HTMLDivElement
    const control = element.querySelector(controlSelector) as HTMLDivElement
    const panel = element.querySelector(panelSelector) as HTMLDivElement
    const optionEls = Array.from(panel.querySelectorAll(optionsSelectors)) as HTMLDivElement[]
    const values = element.querySelector(valuesSelector) as HTMLDivElement | null
    const options: AxelSelectOptions = { ...defaultOptions, ...userOptions }
    const optionsToSelect = options.defaultSelectedOptions
    const previousObj = this.getObject(options.name)

    element.setAttribute('data-axel-select', 'select')

    const switchOptionFn: AxelSelectSwitchOptionFn = (
      optionName, 
      newState = undefined, 
      triggerCallback = false
    ) => {  
      function regenerateSelectedValues() {
        if (!values) return
        if (selectObj.selectedOptions.length === 0) {
          values.innerHTML = options.textEmpty
        } else {
          if (selectObj.options.textFull) {
            values.innerHTML = selectObj.options.textFull
          } else {
            values.innerHTML = ''
            selectObj.selectedOptions.forEach((optionName) => {
              const selectedOption = optionEls.find(
                (optionEl) => optionEl.attributes.getNamedItem('data-axel-select')?.value === optionName
              )
              const generatedOptionEl = document.createElement('span')
              generatedOptionEl.setAttribute('data-axel-select', optionName);
              generatedOptionEl.innerHTML = selectedOption.innerHTML
              values.appendChild(generatedOptionEl)
            })
          }
        }
      }
      function selectOption(optionEl: HTMLDivElement) {
        const isAlreadySelected = selectObj.selectedOptions.includes(optionName)
        if (isAlreadySelected) return
        optionEl.classList.add('axel-active')          
        selectObj.selectedOptions.push(optionName)
        regenerateSelectedValues()
        triggerCallback && onSwitch(optionName, true, selectObj.selectedOptions, optionEl)
      }
      function deselectOption(optionEl: HTMLDivElement) {
        optionEl.classList.remove('axel-active')
        selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName)
        regenerateSelectedValues()
        triggerCallback && onSwitch(optionName, false, selectObj.selectedOptions, optionEl)
      }

			optionEls.forEach(optionEl => {
        if (optionEl.attributes.getNamedItem('data-axel-select')?.value !== optionName) return

        if (newState === true) selectOption(optionEl) 
        else if (newState === false) deselectOption(optionEl) 
        else if (selectObj.selectedOptions.includes(optionName)) deselectOption(optionEl) 
        else selectOption(optionEl)
			})
		}

    const switchOptionsFn: AxelSelectSwitchOptionsFn = (
      optionNames, 
      newState = true, 
      triggerCallbacks = false
    ) => {
      optionEls.forEach((optionEl) => {
        selectObj.selectedOptions = [];
        const optionName = optionEl.attributes.getNamedItem('data-axel-select')?.value
        // switch specified options to given state
        if (optionNames.includes(optionName)) {
          optionEl.classList.toggle('axel-active', newState)

          newState !== false ? 
            selectObj.selectedOptions.push(optionName) : 
            selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName)
        // other options switch to opposite state
        } else {
          optionEl.classList.toggle('axel-active', !newState)
          selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName)
        }

        triggerCallbacks && onSwitch(optionName, newState, selectObj.selectedOptions, optionEl)
      })
    }

    // create object for html select
		const selectObj: AxelSelectObject = {
      element: element,
      options: options,
      selectedOptions: [],
      listeners: null,
			switchOption: switchOptionFn,
      switchOptions: switchOptionsFn,
    }

    // listen to click outside of element 
    const onClickOutsideCallback = onClickOutsideOf(
      element, 
      this.listeners.onClickOutside(selectObj), 
      previousObj?.listeners.onClickOutside
    )
        
    const listeners = {
      onControlClick: this.listeners.onControlClick(selectObj),
      onClickOutside: onClickOutsideCallback,
      options: {}
    }
    
		// set initial state
    if (!optionsToSelect.length) values.innerHTML = options.textEmpty
		else optionsToSelect.forEach((optionName) => selectObj.switchOption(optionName, true));

    // append event listeners for control
		previousObj?.listeners.onControlClick &&
      control.removeEventListener('click', previousObj.listeners.onControlClick)
		control.addEventListener('click', listeners.onControlClick)

    // append event listeners for options
    optionEls.forEach((optionEl) => {
      const optionName = optionEl.getAttribute('data-axel-select')
      listeners.options[optionName] = this.listeners.onOptionClick(selectObj, optionEl, options)
      previousObj?.listeners.options[optionName] && 
        optionEl.removeEventListener('click', previousObj.listeners.options[optionName])
      optionEl.addEventListener('click', listeners.options[optionName])
    })


    selectObj.listeners = listeners

    // add or replace old select object in the static list
    this.allObjects = this.allObjects.filter((object) => object.options.name !== options.name).concat(selectObj);

		return selectObj
  }

  /**
   * Switch option by option name
   */
	static switchOption: AxelSelectSwitchOptionStaticFn = (
    optionName: string,
    state?: boolean, 
    selectName?: string,
  ) => {
    const select = this.getObject(selectName)
    select.switchOption(optionName, state)
  }

  /**
   * Switch multiple options by list of option names
   */
	static switchOptions: AxelSelectSwitchOptionsStaticFn = (
    optionNames: string[],
    state?: boolean, 
    selectName?: string,
  ) => {
    const select = this.getObject(selectName)
    select.switchOptions(optionNames, state)
  }
}