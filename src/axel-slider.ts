import { AxelSliderStatic } from "./axel"
import { AxelSliderObject, AxelSliderOptions, AxelSliderUserOptions } from "./axel-types"


export class AxelSlider extends AxelSliderStatic {
  private static defaultOptions: AxelSliderOptions = {
    name: null,
    startAt: 1
  }

  static init(selector: string, userOptions: AxelSliderUserOptions = {}) {
    let element = document.querySelector(selector) as HTMLDivElement
    let pages = Array.from(element.querySelectorAll('.page')) as HTMLDivElement[]
    let options: AxelSliderOptions = { ...this.defaultOptions, ...userOptions }
    let currentPage = options.startAt

    // append pagination
    element.insertAdjacentHTML('beforeend', `<div class="axel-pagination">
      <button class="axel-arrow axel-left">&#5176;</button>
      <span class="axel-page">
        <span class="axel-current">${currentPage}</span>/<span class="axel-total">${pages.length}</span>
      </span>
      <button class="axel-arrow axel-right">&#5171;</button>
    </div>`);
    
    let slider: AxelSliderObject = {
      element: element,
      options: options,
      pages: pages,
      currentPage: currentPage,
      controls: {
        elArrowLeft: element.querySelector('.axel-pagination .axel-left') as HTMLElement,
        elArrowRight: element.querySelector('.axel-pagination .axel-right') as HTMLElement,
        elCurrentPage: element.querySelector('.axel-pagination .axel-current') as HTMLElement,
        elPageTotal: element.querySelector('.axel-pagination .axel-total') as HTMLElement,
      },
      slideTo: slideTo
    }

    this.allObjects.push(slider);

    // set initial arrows and visible page 
    setArrows()
    setPage()

    // add arrows event listeners
    slider.controls.elArrowLeft.addEventListener('click', (ev) => {
      slider.slideTo(slider.currentPage - 1)
    })
    slider.controls.elArrowRight.addEventListener('click', (ev) => {
      slider.slideTo(slider.currentPage + 1)
    })

    function slideTo(index) {
      if (index <= slider.pages.length && index >= 1) {
        slider.currentPage = index
        slider.controls.elCurrentPage.innerHTML = (index).toString()
        setArrows()
        setPage()
      }      
    }

    function setArrows() {
      if (slider.currentPage === 1) {
        slider.controls.elArrowLeft.classList.add('disabled')
      } else {
        slider.controls.elArrowLeft.classList.remove('disabled')
      }
  
      if (slider.currentPage === slider.pages.length) {
        slider.controls.elArrowRight.classList.add('disabled')
      } else {
        slider.controls.elArrowRight.classList.remove('disabled')
      }
    } 

    function setPage() {
      slider.pages.forEach((pageEl, index) => {
        if (index === slider.currentPage - 1) {
          pageEl.style.display = 'block'
        } else {
          pageEl.style.display = 'none'
        }
      })
    }
    
    return slider
  }

  static slideTo(sliderName: string, slideToPageIndex: number) {
    const slider = this.getObject(sliderName)
    slider.slideTo(slideToPageIndex)
  }
}