import { AxelLayoutStatic } from "./axel";
export class AxelLayout extends AxelLayoutStatic {
    static init(selector, userOptions = {}, onDebouncedResize = () => { }) {
        const defaultOptions = {
            name: null,
            layoutMinHeight: null,
            contentElement: null,
            keepContentMinHeightEqualTo: null,
            debounceTimeInMs: 200
        };
        const options = { ...defaultOptions, ...userOptions };
        const element = document.querySelector(selector);
        const contentElement = options.contentElement ? document.querySelector(options.contentElement) : element;
        const measuredElement = options.keepContentMinHeightEqualTo ? document.querySelector(options.keepContentMinHeightEqualTo) : null;
        const layoutObject = {
            element: element,
            options: options,
            contentElement: contentElement,
            onDebouncedResize: onDebouncedResize
        };
        const updateLayout = () => {
            if (layoutObject.options.layoutMinHeight === 'screen') {
                layoutObject.element.style.minHeight = window.innerHeight + 'px';
            }
            if (typeof layoutObject.options.layoutMinHeight === 'number') {
                layoutObject.element.style.minHeight = options.layoutMinHeight + 'px';
            }
            if (layoutObject.options.keepContentMinHeightEqualTo) {
                if (measuredElement) {
                    layoutObject.contentElement.style.minHeight = this.computeElementHeight(measuredElement) + 'px';
                }
            }
        };
        updateLayout();
        let resizeTimeout;
        window.addEventListener('resize', (event) => {
            clearTimeout(resizeTimeout);
            resizeTimeout = setTimeout(() => {
                updateLayout();
                layoutObject.onDebouncedResize(event);
            }, options.debounceTimeInMs);
        });
        this.allObjects.push(layoutObject);
        return layoutObject;
    }
    static computeElementHeight = (element) => {
        const isDisplayed = window.getComputedStyle(element).display !== 'none';
        if (isDisplayed)
            return element.offsetHeight;
        const wrapper = document.createElement('div');
        wrapper.style.visibility = 'hidden !important';
        wrapper.style.overflow = 'hidden !important';
        wrapper.style.height = '0 !important';
        const clone = element.cloneNode(true);
        clone.style.display = 'block';
        clone.style.height = 'auto';
        wrapper.appendChild(clone);
        element.parentElement?.appendChild(wrapper);
        const height = clone.offsetHeight;
        element.parentElement?.removeChild(wrapper);
        return height;
    };
}
