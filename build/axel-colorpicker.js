import { AxelColorPickerStatic } from "./axel";
export class AxelColorPicker extends AxelColorPickerStatic {
    static init(selector, onSelect, userOptions = {}) {
        const defaultOptions = {
            name: null,
            defaultSelectedColor: null,
            colorGroups: [
                ["#000000", "#808080", "#660033", "#003300", "#000066", "#663300", "#660066", "#003366"],
                ["#ffffff", "#cccccc", "#ff99cc", "#ccffcc", "#9999ff", "#ffffcc", "#ffccff", "#ccffff"]
            ]
        };
        const element = document.querySelector(selector);
        const options = { ...defaultOptions, ...userOptions };
        const colorToSelect = options.defaultSelectedColor;
        const selectColor = (color) => {
            const sanitizedColor = color.toLowerCase();
            colorPickerObj.controls.forEach(control => {
                if (control.attributes.getNamedItem('data-axel-colorpicker')?.value === sanitizedColor) {
                    control.classList.add('axel-active');
                }
                else {
                    control.classList.remove('axel-active');
                }
            });
        };
        const controls = [];
        options.colorGroups.forEach(colorGroup => {
            const colorGroupElement = document.createElement('div');
            colorGroupElement.style.cssText = 'display: flex;';
            colorGroup.forEach(color => {
                const sanitizedColor = color.toLowerCase();
                const colorElement = document.createElement('span');
                colorElement.setAttribute('data-axel-colorpicker', sanitizedColor);
                colorElement.style.cssText = 'min-width: 10px; min-height: 10px; cursor: pointer;';
                colorElement.style.backgroundColor = sanitizedColor;
                controls.push(colorElement);
                colorGroupElement.appendChild(colorElement);
            });
            element.appendChild(colorGroupElement);
        });
        const colorPickerObj = {
            element: element,
            controls: controls,
            options: options,
            selectedColor: colorToSelect,
            selectColor: selectColor
        };
        colorToSelect && selectColor(colorToSelect);
        colorPickerObj.controls.forEach(el => {
            el.addEventListener('click', (ev => {
                const color = el.attributes.getNamedItem('data-axel-colorpicker')?.value || null;
                selectColor(color);
                onSelect(color, colorPickerObj.options.name);
            }));
        });
        this.allObjects.push(colorPickerObj);
        return colorPickerObj;
    }
    static selectColor(colorPickerName, color) {
        const colorPicker = this.getObject(colorPickerName);
        colorPicker.selectColor(color);
    }
}
