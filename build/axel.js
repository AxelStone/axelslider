export class AxelElementBaseStatic {
    static allObjects = [];
    static _getObjects(names) {
        if (!names)
            return this.allObjects;
        return this.allObjects.filter((object) => names.includes(object.options.name));
    }
    static _getObject(name) {
        if (!name)
            return this.allObjects[0];
        return this.allObjects.find((object) => object.options.name === name);
    }
}
export class AxelLayoutStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelSwitcherStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelSliderStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelTabsStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelAccordionStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelSelectStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
export class AxelColorPickerStatic extends AxelElementBaseStatic {
    static getObject = (this._getObject);
    static getObjects = (this._getObjects);
}
