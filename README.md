# Axel UI
Simple UI elements in vanilla JS/TS.

All elements are initialized through element's static class ('Element' word is a placeholder):

`const axelElementObject: AxelElementObject = AxelElement.init(name, options)`

where every UI element's static class implements public static methods:

```typescript
type AxelElementClass = {
  // Initialize new UI element object 
  init: (selector: string, options: AxelElementOptions) => AxelElementObject
  // Get single initialized object, if name is not specified returns first initialized object
  getObject: (name?: string) => AxelElementObject | undefined
  // If names is undefined returns all objects
  getObjects: (names?: string[]) => AxelElementObject[] 
  // Another functions specific for given UI element class (e.g. `switchTab`, `closeAllTabs`)
  [functionName: string]: ( 
    // name of the initilaized UI element's object on which this function will be called
    name: string, 
    // parameters for UI element's object function with the same name
    params: any 
  )
}
```

and all initialized UI element objects implements:

```typescript
type AxelElementObject = {
  // Main DOM element of initialized object
  element: HTMLElement
  // Options defining behaviour aand initial state
  options: AxelElementOptions 
  // State informations, e.g `openedTabs`, `selectedColor`, etc.
  [stateInfo: string]: any 
  // Important elements, e.g. `controls`, `tabs`, etc.
  [innerElements: string]: HTMLElement | HTMLElement[] 
  // Control functions, e.g. `switchTab`, `closeAllTabs`, etc.
  [functionName: string]: (...params: any[]) => any 
}
```

Therefore, all control functions are attached to initialized objects and
also are accesible through element's static class:

```javascript
// use object functions and state attached to initialized object directly
const axelElemObj = AxelElement.init('#axel-element')
const state = axelElemObj.stateVariable
axelElemObj.functionName(...params)

// or call the functions of the initialized object through static class
AxelElement.init('#axel-element')
AxelElement.functionName(objectName, ...params)

// or get the object through static class and access it's functions and state
const axelObject = AxelElement.getObjects([objectName])
const state = axelObject.stateVariable
axelObject.functionName(params)
```

### Example:
```javasscript
AxelSlider.init('#slider-1', { name: 'slider-1', startAt: 3 })
const slider2 = AxelSlider.init('#slider-2', { name: 'slider-2' })

AxelSlider.slideTo('slider-2', 2)
slider2.slideTo(2)

const slider1 = AxelSlider.getObject('slider-1')
slider1.slideTo(2)
```

### Building and importing
In `src/index.ts` comment unnecessary classes for your project and run `npm run build`,
which will generate bundled `dist/axelui.js` that can be imported in html:

```html
<script src="dist/axelui.js"></script>
...
<script>
  AxelElement.init(selector)
</script>
```

Or run `npm run build:ts` in order to not bundle the files into one file. Then it can be used
as ES module:

```html
<script type="module" src="build/axel.js"></script>
<script type="module" src="build/axel-slider.js"></script>
<script type="module">
  import { AxelSlider } from './build/axel-slider.js'
  AxelSlider.init('#slider')
</script>
```

### Common types 
Every UI element extends these base types:

```javascript
/**
 * Initialized Axel UI element's object base
 */
type AxelElementObjectBase = {
  // Main HTML DOM element of initialized UI object
  element: HTMLElement 
  // options passed to initialized UI object
  options: AxelElementOptionsBase 
}

/**
 * Options defining behaviour and a state of the initialized object
 */
type AxelElementOptionsBase = {
  // Useful in case of multiple initialized UI elements of the same class 
  name: string | null 
}
```

## AxelLayout
Drives HTML layout properties according to screen size and resizing.

Initialization:

```html
<div id="app-wrapper">
  <!-- Optional -->
  <div id="app-content">
    ...
    <!-- Optional, can be hidden -->
    <div id="height-defining-element">
  </div>
</div>
```

```typescript
AxelLayout.init(
  selector: string, 
  options?: AxelLayoutOptions,
  // callback executed on window resizing (debounced)
  onDebouncedResize?: (event: Event) => void
): AxelLayoutObject

type AxelLayoutObject = AxelElementObjectBase & {
  options: AxelLayoutOptions,
  contentElement: HTMLElement,
  onDebouncedResize: (event: Event) => void
}
type AxelLayoutOptions = AxelElementOptionsBase & {
  /**
   * `screen` - keep layout min-height always equal to screen height.
   * Useful if there is @viewport width rule in css (vh units doesn't work properly in that case)
   * 
   * number - exact height in pixels
   */
  layoutMinHeight: 'screen' | number | null,
  /**
   * Element on which the the `keepContentMinHeightEqualTo` is applied.
   * If not specified, it is the same as main element.
   */
  contentElement: string | null,
  /**
   * CSS selector of element that specifies the height of contentElement.
   * Even if this element is hidden the height of it will be calculated as if it is not hidden.
   * 
   * Useful for example when there are tabs and we want to keep the min-height of tab wrapper
   * equal to specified tab even if the tab is hidden (prevents content skipping)
   */
  keepContentMinHeightEqualTo: string | null
  /**
   * Debounce time used when resizing - `onDebouncedResize` is called after this time
   */
  debounceTimeInMs: number
}
```

## AxelSwitcher
Simple binary or ternary switcher.

Even if `options.mode` is `binary` it is possible to set `defaultState` to `neutral` or switch programatically to `neutral`

```html
<!-- Class `page` is mandatory. -->
<div id="mySwitcher">
  <!-- This can be defined directly in HTML, otherwise it is autogenerated -->
  <span data-axel-switcher="control">..</span>
</div>
```

```typescript
declare class AxelSwitcher extends AxelSwitcherStatic {
  init(selector: string, onSwitch: (state: string) => void, options: AxelSwitcherOptions): AxelSwitcherObject
  switch(state?: 'on' | 'neutral' | 'off'): void
}
type AxelSwitcherObject = AxelElementObjectBase & {
  options: AxelSwitcherOptions
  control: HTMLElement
  state: 'on' | 'off' | 'neutral'
  switch: (state?: 'on' | 'neutral' | 'off') => 'on' | 'off' | 'neutral'
}
type AxelSwitcherOptions = AxelElementOptionsBase & {
  defaultState: 'off' | 'on' | 'neutral' // default - 'off'
  mode: 'binary' | 'ternary' // default - 'binary'
}
```

## AxelSlider
Simple slider.

```html
<!-- Class `page` is mandatory. -->
<div id="mySlider">
  <div data-axel-slider="page">..</div>
  <div data-axel-slider="page">..</div>
  <div data-axel-slider="page">..</div>
  <!-- This is autogenerted -->
  <div class="axel-pagination">
    <button class="axel-arrow axel-left">&#5176;</button>
    <span class="axel-page">
      <span class="axel-current">${currentPage}</span>/<span class="axel-total">${pages.length}</span>
    </span>
    <button class="axel-arrow axel-right">&#5171;</button>
  </div>
</div>
```

```javascript
type AxelSliderObject = AxelElementObjectBase & {
  options: AxelSliderOptions
  currentPage: number
  pages: HTMLDivElement[]
  controls: {
    elArrowLeft: HTMLElement
    elArrowRight: HTMLElement
    elCurrentPage: HTMLElement
    elPageTotal: HTMLElement
  }
  slideTo: (index: number) => void
}
type AxelSliderOptions = AxelElementOptionsBase & {
  startAt: number
}
```

## AxelTabs
Simple tabs.

```html
<!-- Attribute values `controls`, `panels` and `tab-*` are mandatory. -->
<div id="my-tabs">
  <div data-axel-tabs="controls">
    <!-- Active element will get class 'axel-active' -->
    <h3 data-axel-tabs="tab-1">..</h3>
    <h3 data-axel-tabs="tab-2">..</h3>
    <h3 data-axel-tabs="tab-3">..</h3>
  </div>
  <div data-axel-tabs="panels">
    <!-- Active element will get class 'axel-active' -->
    <div data-axel-tabs="tab-1">..</div>   
    <div data-axel-tabs="tab-2">..</div>
    <div data-axel-tabs="tab-3">..</div>
  </div> 
</div> 
```

```typescript
type AxelTabsObject = AxelElementObjectBase & {
  options: AxelTabsOptions
  controls: HTMLDivElement[]
  panels: HTMLDivElement[]
  openedTabName: string | null
  openTab: (tabName: string) => void
}
type AxelTabsOptions = AxelElementOptionsBase & {
  defaultOpenedTab: string | null
}
```

## AxelAccordion
Simple accordion.

```html
<!-- Attribute values `tab-*`, `control` and `panel` are mandatory. -->
<div id="my-accordion">
  <!-- Active tab element will get class 'axel-active' -->
  <div data-axel-accordion="tab-one">
    <h3 data-axel-accordion="control">..</h3>
    <div data-axel-accordion="panel">..</div>
  </div>
  <div data-axel-accordion="tab-two">
    <h3 data-axel-accordion="control">..</h3>
    <div data-axel-accordion="panel">..</div>
  </div>
</div> 
```

```typescript
type AxelAccordionObject = AxelElementObjectBase & {
  options: AxelAccordionOptions
  openedTabNames: string[]
  tabs: HTMLDivElement[]
  switchTab: (tabName: string) => void
  closeAllTabs: () => void
}
type AxelAccordionOptions = AxelElementOptionsBase & {
  allowMultiple: boolean
  defaultOpenedTabs: string[]
}
```


## AxelSelect
Simple select with multiselection.

"control" element is opening "panel" element with options.
"values" element (optional) contains autogenerated selected options or `textEmpty` string from options.

```html
<!--
 Attribute values `control`, `panel` and `option-*` are mandatory.
 Attribute `data-axel-select="select"` is autogenerated if not specified.
 Opened select will get class `axel-active` 
-->
<div id="my-select" data-axel-select="select">
  <!-- 
    This can be merged into one element: 
    <div data-axel-select="control" data-axel-select="values"><div>
  -->
  <div data-axel-select="control">
    <!-- Optional -->
    <div data-axel-select="values"></div>
  </div>

  <div data-axel-select="panel">
    <!-- Selected element will get class `axel-active` -->
    <div data-axel-select="option-1">Option content 1</div>
    <div data-axel-select="option-2">Option content 2</div>
  </div>
</div>
```

```typescript
type AxelSelect = {
  init: (
    selector: string,
    userOptions?: AxelSelectUserOptions,
    onSwitch?: (optionName: string, newState: boolean, selectedOptions: string[]) => void
  ) => AxelSelectObject
  switchOption: (optionName: string, state?: boolean, name?: string) => void
}
type AxelSelectObject = AxelElementObjectBase & {
  options: AxelSelectOptions
  selectedOptions: string[]
  listeners: {
    onControlClick: () => void
    onClickOutside: (event: MouseEvent) => void
    options: Record<string, () => void>
  }
  switchOption: AxelSelectSwitchOptionFn
  switchOptions: AxelSelectSwitchOptionsFn
}
type AxelSelectUserOptions = AxelElementOptionsBase & {
  allowMultiple?: boolean // default: true
  closeOnSelect?: boolean // default: false
  textEmpty?: string // default: "- select -"
}
```

## AxelColorPicker
Simple colorpicker.

```html
<div id="my-colorpicker">
  <!-- All inner elements are autogenerated -->
  <!-- color group (row) -->
  <div style="display: flex">
    <!-- colors -->
    <div data-axel-colorpicker="#ffffff" style="display: inline-block;"></div>
    <div data-axel-colorpicker="#000000" style="display: inline-block;"></div>
    ...
  </div>
</div> 
```

```typescript
type AxelColorPickerObject = AxelElementObjectBase & {
  options: AxelColorPickerOptions,
  controls: HTMLElement[],
  selectedColor: string | null,
  selectColor: (color: string) => void
}
type AxelColorPickerOptions = AxelElementOptionsBase & {
  colorGroups: string[][]
  defaultSelectedColor: string | null
}
```
